package com.bergmann.matthias.androidgame;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;


public class Drawing {


    private Bitmap background;
    private Bitmap pauseIcon;
    Paint paint = new Paint();
    Game game;

    public Drawing(Bitmap background,Bitmap pauseIcon) {
        // height is also width/10 because icon is distorted otherwise

        this.background = Bitmap.createScaledBitmap(background, GamePanel.WIDTH, GamePanel.HEIGHT, false);
        this.pauseIcon = Bitmap.createScaledBitmap(pauseIcon, GamePanel.WIDTH/18, GamePanel.HEIGHT/15, false);
        game = new Game();
    }
    public void showFPS(Canvas canvas, double FPS) {

        paint.setColor(Color.WHITE);
        paint.setTextSize(30);
        paint.setFakeBoldText(true);
        paint.setLinearText(true);
        canvas.drawText(FPS + " FPS",30,45,paint);

    }

    public void drawScore(Canvas canvas, long score) {

        paint.setColor(Color.WHITE);
        paint.setTextSize(45);
        paint.setFakeBoldText(true);
        paint.setLinearText(true);
        paint.setTypeface(Typeface.create("Arial",Typeface.ITALIC));
        canvas.drawText("score: " + score,GamePanel.WIDTH/2-120,45,paint);
    }

    public void drawHighscore(Canvas canvas,int highscore) {
        paint.setColor(Color.WHITE);
        paint.setTextSize(45);
        paint.setFakeBoldText(true);
        paint.setLinearText(true);
        paint.setTypeface(Typeface.create("Arial",Typeface.ITALIC));
        canvas.drawText("Highscore: " + highscore,GamePanel.WIDTH-420,45,paint);
    }

    public void drawBackground(Canvas canvas){
      canvas.drawBitmap(background, 0, 0, null);
    }

    public void drawPauseIcon(Canvas canvas) {
        canvas.drawBitmap(pauseIcon,GamePanel.WIDTH - 90, 10 ,null);
    }


}
