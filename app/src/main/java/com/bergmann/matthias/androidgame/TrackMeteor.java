package com.bergmann.matthias.androidgame;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Admin on 18.04.2017.
 */

public class TrackMeteor {

    private Bitmap trackMeteor;
    private int trackX = 0;
    private int trackY = -500;
    private int speed = 5;

    private Rect trackHitbox;
    private Rect hitboxPlayerHead;
    private Rect hitboxPlayerBody;
    Paint paint = new Paint();

    public TrackMeteor(Bitmap trackMeteor) {
        this.trackMeteor = trackMeteor;
    }

    public void createTrackMeteor() {
        trackX = (int)Player.x + 15;
        trackY = -100;
        GamePanel.trackMeteorActive = true;

    }

    public void updateTrackHitbox() {
        trackHitbox = new Rect(trackX + 30,trackY + 30,trackX + 155, trackY + 155);
        hitboxPlayerHead = GamePanel.hitboxPlayerHead;
        hitboxPlayerBody = GamePanel.hitboxPlayerBody;
    }

    public void moveTrackMeteor() {
        trackY += speed;

    }

    public void testOutside() {
        if (trackY > GamePanel.HEIGHT){
            GamePanel.trackMeteorActive = false;
        }
    }

    public void testHitPlayer() {
        if (hitboxPlayerHead.intersect(trackHitbox) || hitboxPlayerBody.intersect(trackHitbox)) {
            GamePanel.death();

        }
    }

    public void draw(Canvas canvas) {

        canvas.drawBitmap(trackMeteor, trackX, trackY, null);



    }
}