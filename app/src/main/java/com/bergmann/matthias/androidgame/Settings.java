package com.bergmann.matthias.androidgame;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;

public class Settings extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_settings);
    }

    public void onClose(View view) {
        startActivity(new Intent(this, MainMenu.class));
    }

    public void onOneHand(View view) {
        MainMenu.oneHand();
    }

    public void onTwoHand(View view) {
        MainMenu.twoHand();
    }

    public void changeProgress(SeekBar seekBar, int progress){
        try {
            int[] imageID = {R.drawable.vol0, R.drawable.vol1, R.drawable.vol2, R.drawable.vol3, R.drawable.vol4, R.drawable.vol5};
            seekBar.setBackground(ContextCompat.getDrawable(this, imageID[progress]));
            Sounds.setVolume(progress, Sounds.maxVolume());
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
