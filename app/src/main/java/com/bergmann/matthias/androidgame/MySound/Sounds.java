package com.bergmann.matthias.androidgame.MySound;

import android.media.MediaPlayer;

import com.bergmann.matthias.androidgame.Menu.MainMenu;

public class Sounds {
    private static MediaPlayer mp;
    private static int maxVolume = 5;
    public static int maxVolume(){return maxVolume;}

    public static void setVolume(int vol, int maxVol){
        float volume = (float) (1 - (Math.log(maxVol - vol) / Math.log(maxVol)));
        mp.setVolume(volume, volume);
    }

    public static void startIngameSound() {

        mp = MainMenu.ingameSound;
        mp.setLooping(true);
        mp.start();
    }
    public static void stopIngameSound() {

        mp = MainMenu.ingameSound;
        mp.stop();
        mp.setLooping(false);
    }

    public static boolean isPlaying() {
        return mp.isPlaying();
    }

    public static void stopMP() {
        mp.stop();
        mp.setLooping(true);
    }

    public static void playDeathSound() {

        mp = MainMenu.deathSound;
        mp.start();

    }

    public static void playStartScreenSound() {


        mp = MainMenu.startscreensound;
        mp.setLooping(true);
        mp.start();
    }

    public static void stopStartScreenSound() {

        mp = MainMenu.startscreensound;
        mp.stop();
        mp.setLooping(false);
    }




}
