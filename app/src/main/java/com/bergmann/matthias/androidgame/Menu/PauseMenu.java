package com.bergmann.matthias.androidgame.Menu;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bergmann.matthias.androidgame.R;

public class PauseMenu extends Activity {

    public static boolean pause = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pause_menu);
        pause = true;
        this.overridePendingTransition(R.anim.pop_in,R.anim.slide_out);
    }

    public void onExit(View view) {
        finish();
        System.exit(0);

    }

    @Override
    public void finish() {
        super.finish();
        this.overridePendingTransition(R.anim.pop_in,R.anim.slide_out);
    }

    public void onSettings(View view) {
        finish();
        startActivity(new Intent(this,Settings.class));
    }

    public void onClosePauseMenu(View view) {
        //view.setBackgroundResource(R.drawable.pause_menue_closebutton_clicked);
        PauseMenu.pause = false;
        finish();

    }
}
