package com.bergmann.matthias.androidgame.GUI.Character;

import android.graphics.Rect;


public class updateHitboxPlayer {

    public static Rect updateHitboxPlayerHead(Player player) {
        int angle = player.angle;

        int x = (int) ((Math.sin(Math.toRadians(angle))*-30) + player.x + player.getWidth()/2);
        int y = (int) ((Math.cos(Math.toRadians(angle))*40) + player.y + player.getHeight()/2);
        return new Rect(x-45, y-45, x+45, y+45);


    }

    public static Rect updateHitboxPlayerBody(Player player) {
        int angle = player.angle;

        int x = (int) ((Math.sin(Math.toRadians(angle))*30) + player.x + player.getWidth()/2);
        int y = (int) ((Math.cos(Math.toRadians(angle))*-40) + player.y + player.getHeight()/2);
        return new Rect(x-45, y-45, x+45, y+45);

    }
}
