package com.bergmann.matthias.androidgame.Menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.bergmann.matthias.androidgame.R;

public class Settings extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_settings);
        overridePendingTransition(R.anim.pop_in,R.anim.pop_out);
    }

    @Override
    public void finish() {
        super.finish();
        this.overridePendingTransition(R.anim.pop_in,R.anim.pop_out);
    }

    public void onClose(View view) {
        this.finish();

        if(PauseMenu.pause) {
            startActivity(new Intent(this, PauseMenu.class));
        }

    }

    public void onOneHand(View view) {
        Toast.makeText(getApplicationContext(), "One Handed Control Acitve now", Toast.LENGTH_SHORT).show();
        MainMenu.oneHand();
    }

    public void onTwoHand(View view) {
        Toast.makeText(getApplicationContext(), "Two Handed Control Acitve now", Toast.LENGTH_SHORT).show();
        MainMenu.twoHand();
    }

  /*  public void changeProgress(SeekBar seekBar, int progress){
        try {
            int[] imageID = {R.drawable.vol0, R.drawable.vol1, R.drawable.vol2, R.drawable.vol3, R.drawable.vol4, R.drawable.vol5};
            seekBar.setBackground(ContextCompat.getDrawable(this, imageID[progress]));
            Sounds.setVolume(progress, Sounds.maxVolume());
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    */
}
