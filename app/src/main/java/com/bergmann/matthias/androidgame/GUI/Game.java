package com.bergmann.matthias.androidgame.GUI;

import   android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.bergmann.matthias.androidgame.Menu.MainMenu;
import com.bergmann.matthias.androidgame.Menu.PauseMenu;
import com.bergmann.matthias.androidgame.MySound.Sounds;


public class Game extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(new GamePanel(this,this));

    }

    public void startPauseScreen() {
        startActivity(new Intent(this,PauseMenu.class));
    }


    public void stop(){
        startActivity(new Intent(this, MainMenu.class));
    }

    @Override
    public void onBackPressed() {
       //Test if user wants to exit
        startActivity(new Intent(this,PauseMenu.class));
    }

    @Override
    protected void onDestroy() {
        Sounds.stopIngameSound();
    }
}
