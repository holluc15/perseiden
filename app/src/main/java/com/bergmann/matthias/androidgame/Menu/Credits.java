package com.bergmann.matthias.androidgame.Menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bergmann.matthias.androidgame.Menu.MainMenu;
import com.bergmann.matthias.androidgame.R;

public class Credits extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
        overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
    }

    public void onBack(View view) {

        this.finish();
        //startActivity(new Intent(this, MainMenu.class));
    }

    @Override
    public void finish() {
        super.finish();
        this.overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
    }
}
