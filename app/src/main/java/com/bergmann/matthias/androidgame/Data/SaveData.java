package com.bergmann.matthias.androidgame.Data;

import android.content.Context;
import android.content.SharedPreferences;

public class SaveData {

    public static final String PREFERENCE_NAME = "PREFERENCE_DATA";
    private final SharedPreferences sharedpreferences;

    public SaveData(Context context) {
        sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public int getHighscore() {
        int count = sharedpreferences.getInt("KEY", 0);
        return count;
    }

    public void setHighscore(int count) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("KEY", count);
        editor.commit();
    }

    public void clearCount() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("KEY");
        editor.commit();
    }



}
