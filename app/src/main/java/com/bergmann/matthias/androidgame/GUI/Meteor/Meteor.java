package com.bergmann.matthias.androidgame.GUI.Meteor;


import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.Random;

import android.graphics.Paint;
import android.graphics.Rect;

import com.bergmann.matthias.androidgame.GUI.GamePanel;
import com.bergmann.matthias.androidgame.Thread.MainThread;


public class Meteor {

    private ArrayList<Integer> meteorX;
    private ArrayList<Integer> meteorY;
    private ArrayList<Integer> speedX;
    private ArrayList<Rect> meteorHitbox;
    private ArrayList<Integer>type;


    private Rect hitboxPlayerHead;
    private Rect hitboxPlayerBody;

    private int count = 4;
    public static int speed = 5; // speed could be changed
    private int screenW;
    private int screenH;
    private int FPS;
    Paint paint = new Paint();


    Random rand = new Random();

    private Bitmap meteor1;
    private Bitmap meteor2;
    private Bitmap meteor3;
    private Bitmap meteor4;


    public Meteor(Bitmap meteor1,Bitmap meteor2,Bitmap meteor3,Bitmap meteor4, int screenWidth, int screenHeight, int FPS){
        this.meteor1 = Bitmap.createScaledBitmap(meteor1, screenWidth/8, screenWidth/8, false);
        this.meteor2 = Bitmap.createScaledBitmap(meteor2,screenWidth/6,screenHeight/4,false);
        this.meteor3 = Bitmap.createScaledBitmap(meteor3,screenWidth/6,screenHeight/4,false);
        this.meteor4 = Bitmap.createScaledBitmap(meteor4, screenWidth/8, screenWidth/8, false);

        screenW = screenWidth;
        screenH = screenHeight;

        meteorX = new ArrayList<>();
        meteorY = new ArrayList<>();
        speedX = new ArrayList<>();
        meteorHitbox = new ArrayList<>();
        type = new ArrayList<>();
        this.FPS = FPS;



    }

    public void moveMeteor() {


        hitboxPlayerHead = GamePanel.hitboxPlayerHead;
        hitboxPlayerBody = GamePanel.hitboxPlayerBody;

        for (int j = 0; j < meteorX.size(); j++) {
            meteorX.set(j, meteorX.get(j) + speedX.get(j));
            meteorY.set(j, meteorY.get(j) + speed);
            switch(type.get(j)) {
                case 1: meteorHitbox.set(j,new Rect(meteorX.get(j) +30,meteorY.get(j) +30,meteorX.get(j) +155,meteorY.get(j) +155)); break;
                case 2: meteorHitbox.set(j,new Rect(meteorX.get(j) +80,meteorY.get(j) +80,meteorX.get(j) +170,meteorY.get(j) +170)); break;
                case 3: meteorHitbox.set(j,new Rect(meteorX.get(j) +70,meteorY.get(j) +45,meteorX.get(j) +230,meteorY.get(j) +205)); break;
                case 4: meteorHitbox.set(j,new Rect(meteorX.get(j) +80,meteorY.get(j) +80,meteorX.get(j) +170,meteorY.get(j) +170)); break;
                default: System.err.println("ERROR - type " + type.get(j) + "couldn't be set! (moveMeteor method)");
            }

        }

        testOutside();


    }

    public void testOutside() {
        // test if meteor is outside
        // iterate through every value and if meteory is larger than screenheight remove that value
        for (int i = 0; i < meteorY.size(); i++) {
            if (meteorY.get(i) > screenH){
                meteorX.remove(i);
                meteorY.remove(i);
                speedX.remove(i);
                type.remove(i);

            }
        }
    }


    public void testHitPlayer() {
        // Here comes the test if a meteor hits a player
        for(int i = 0;i < meteorX.size() ;i++ ) {
            if (hitboxPlayerHead.intersect(meteorHitbox.get(i)) || hitboxPlayerBody.intersect(meteorHitbox.get(i))) {
                GamePanel.death();
            }

        }

    }

    public void createMeteor() {

        if (count >= MainThread.difficulty) {

            type.add(rand.nextInt(4) + 1);

            meteorX.add(rand.nextInt(screenW) - 1); // width of the phone should is entered here
            meteorY.add(-meteor1.getHeight());
            speedX.add(rand.nextInt(2 * speed) - speed);
            meteorHitbox.add(new Rect());

            count = 0;
        }
        count++;

    }

    public void draw(Canvas canvas) {

        for (int i = 0; i < meteorX.size(); i++) {
            //drawing the meteors

            switch (type.get(i)) {
                case 1:
                    canvas.drawBitmap(meteor1, meteorX.get(i), meteorY.get(i), null);
                    break;
                case 2:
                    canvas.drawBitmap(meteor2, meteorX.get(i), meteorY.get(i), null);
                    break;
                case 3:
                    canvas.drawBitmap(meteor3, meteorX.get(i), meteorY.get(i), null);
                    break;
                case 4:
                    canvas.drawBitmap(meteor4, meteorX.get(i), meteorY.get(i), null);
                    break;
                default:
                    System.err.println("WRONG TYPE!! - type " + type + " isn't possible(meteorDraw method)");



            }


        }


    }




}


