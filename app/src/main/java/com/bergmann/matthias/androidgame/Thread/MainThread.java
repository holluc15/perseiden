package com.bergmann.matthias.androidgame.Thread;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

import com.bergmann.matthias.androidgame.GUI.GamePanel;
import com.bergmann.matthias.androidgame.Menu.PauseMenu;
import com.bergmann.matthias.androidgame.MySound.Sounds;

/*
the mainthread handles the fps and calls the update and draw method of the gamePanel
 */

public class MainThread extends Thread{

    public static int FPS = 30;
    public static int difficulty = 75;
    public static int gamePoints;
    public static boolean running;
    private final SurfaceHolder surfaceHolder;
    private GamePanel gamePanel;
    public static Canvas canvas;
    public static double averageFPS;
    public static int score = 0;
    public static int multiplier = 1;
    private int zeroCounter = 0;



    public MainThread(SurfaceHolder surfaceHolder, GamePanel gamePanel){
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;


    }

    public void run(){
        long startTime;
        long waitTime;
        long timeMillis;
        long totalTime = 0;
        long targetTime = 1000/FPS;
        int frameCount = 0;


        //start ingame sound
        Sounds.startIngameSound();
        while (running){


                try {
                    while(PauseMenu.pause) {
                        sleep(10);

                    }
                   // GamePanel.startGameAgain();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }



            startTime = System.nanoTime();
            canvas = null;

            // try locking canvas for pixel editing
            try{
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder){

                    gamePanel.update();
                    gamePanel.draw(canvas);
                }
            }catch (Exception e){e.printStackTrace();}
            finally {
                if(canvas != null){
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }catch (Exception e){e.printStackTrace();}
                }
            }

            timeMillis = (System.nanoTime() - startTime)/1000000;
            waitTime = targetTime - timeMillis;

            try {
                if (waitTime > 0) {
                    Thread.sleep(waitTime);
                }
            }catch (Exception e){e.printStackTrace();}

            totalTime += System.nanoTime() - startTime;
            frameCount++;

            if (frameCount == FPS){
                averageFPS = 1000/((totalTime / frameCount)/ 1000000);
                totalTime = 0;
                frameCount = 0;
                score += multiplier;


                int touchcounter = gamePanel.getTouchCounter();
                gamePanel.resetTouchTimer();

                if(touchcounter == 0) {
                    zeroCounter++;
                }else{

                    zeroCounter = 0;
                }

                if(zeroCounter >= 10) {
                    GamePanel.trackMeteorActive = true;
                    zeroCounter = 0;
                }


                switch (gamePoints) {

                    case 15:  difficulty = 75;    multiplier = 1; break;
                    case 25:  difficulty = 60;    multiplier = 2;  break;
                    case 50:  difficulty = 45;    multiplier = 3; break;
                    case 100: difficulty = 30;    multiplier = 4; break;
                    case 200: difficulty = 15;    multiplier = 5; break;

                }
                gamePoints++;

            }
        }

        gamePanel.getGame().stop();
    }

    public void setRunning(boolean b){
        running = b;
    }


}
