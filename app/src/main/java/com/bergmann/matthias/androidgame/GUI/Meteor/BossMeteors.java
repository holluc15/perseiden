package com.bergmann.matthias.androidgame.GUI.Meteor;


import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.LinkedList;
import java.util.Random;

import android.graphics.Rect;

import com.bergmann.matthias.androidgame.GUI.Character.Player;
import com.bergmann.matthias.androidgame.GUI.GamePanel;


public class BossMeteors {

    Random rand = new Random();
    private int bossX = GamePanel.WIDTH/2;
    private int bossY = -500;
    private LinkedList<Rect> meteorHitbox = new LinkedList<>();
    private int type;
    public static boolean bossfight = false;

    private Rect hitboxPlayerHead;
    private Rect hitboxPlayerBody;

    private int count;
    private int speed = 10; // speed could be changed
    private int screenH = GamePanel.HEIGHT;
    private int direction = 1;
    private int downs = 0;
    private int timerStage3 = 0;
    private int rarity = 1500;

    private Bitmap boss1;
    private Bitmap boss2;
    private Bitmap boss3;
    private Bitmap boss4;



    public BossMeteors(Bitmap boss1,Bitmap boss2,Bitmap boss3,Bitmap boss4){
        this.boss1 = Bitmap.createScaledBitmap(boss1, 450, 450, false);
        this.boss2 = Bitmap.createScaledBitmap(boss2, 450, 450, false);
        this.boss3 = Bitmap.createScaledBitmap(boss3, 250, 250, false);
        this.boss4 = Bitmap.createScaledBitmap(boss4, 1250, 1250, false);

    }

    public void moveBossMeteor() {
        hitboxPlayerHead = GamePanel.hitboxPlayerHead;
        hitboxPlayerBody = GamePanel.hitboxPlayerBody;

        switch (type) {
            case 1:
                if(direction == 1)
                {
                    bossX += speed;
                }else{

                    bossX -= speed;
                }
                bossY += speed/2;

                if(count <= 150) {
                    direction = 1;
                }
                if(count > 150) {
                    direction = 2;
                }
                if(count >= 300) {
                    count = 0;
                }
                count++;
                break;

            case 2: bossY += 20; break;

            case 3:
                timerStage3++;

                if(timerStage3 <= 350) {
                    if (Player.x > bossX) {
                        bossX += speed / 3;
                    }
                    if (Player.x < bossX) {
                        bossX -= speed / 3;
                    }
                    if (Player.y > bossY) {
                        bossY += speed / 3;
                    }
                    if (Player.y < bossY) {
                        bossY -= speed / 3;
                    }
                }else{
                    bossY += 15;
                }

                break;
            case 4:
                bossY += 10;

                break;


        }

        testOutside();


    }

    public void testOutside() {
        // test if meteor is outside
        // iterate through every value and if meteory is larger than screenheight remove that value

        switch(type) {
            case 1:
                if (bossY - 175 >= screenH)
                {
                    BossMeteors.bossfight = false;
                    resetCoordinates();

                }

                if(bossX + 275>= GamePanel.WIDTH) {
                    count = 151;
                }

                if(bossX + 275<= 0) {
                    count = 0;
                }
                break;
            case 2:
                if (bossY - 275 >= screenH)
                {
                    downs++;
                    bossY = -500;
                    bossX = (int)Player.x;

                }
                if(downs >= 2) {

                    BossMeteors.bossfight = false;
                    resetCoordinates();
                    downs = 0;

                }
                break;
            case 3:
                if (bossY - 275 >= screenH)
                {
                    BossMeteors.bossfight = false;
                    direction = 1;
                    timerStage3 = 0;
                    resetCoordinates();

                }

                if(bossX + 275>= GamePanel.WIDTH)
                {
                    count = 151;
                }

                if(bossX + 275<= 0) {
                    count = 0;
                }
                break;
            case 4:
                if(bossY >= GamePanel.HEIGHT) {
                    BossMeteors.bossfight = false;
                    resetCoordinates();

                }

        }


    }

    public void updateBossHitbox() {
        meteorHitbox.clear();

        switch(type) {
            case 1: meteorHitbox.add(new Rect(bossX +50, bossY +50, bossX + 390, bossY + 390)); break;
            case 2: meteorHitbox.add(new Rect(bossX + 75, bossY + 50, bossX + 380, bossY + 365)); break;
            case 3: meteorHitbox.add(new Rect(bossX + 55, bossY + 55, bossX + 215, bossY + 215)); break;
            case 4:
                meteorHitbox.add(new Rect(bossX + 185, bossY + 185, bossX + 1050, bossY + 1050));
                meteorHitbox.add(new Rect(bossX + 315, bossY + 50, bossX + 895, bossY + 500));
                meteorHitbox.add(new Rect(bossX + 215, bossY + 945, bossX + 950, bossY + 1150));
                meteorHitbox.add(new Rect(bossX + 65, bossY + 250, bossX + 285, bossY + 900));
               // meteorHitbox.add(new Rect(bossX + 965, bossY + 250, bossX + 1285, bossY + 900));
                break;

        }
    }

    public void resetCoordinates() {
        bossX = GamePanel.WIDTH/2;
        bossY = -500;
    }


    public void testHitPlayer() {
        // Here comes the test if a meteor hits a player

            for(int i = 0; i < meteorHitbox.size(); i++) {
                if (hitboxPlayerHead.intersect(meteorHitbox.get(i)) || hitboxPlayerBody.intersect(meteorHitbox.get(i))) {
                    GamePanel.death();

                }
            }


    }


    public void createMeteor() {

            switch (rand.nextInt(4) + 1) {
                case 1:
                    type = 1;
                    break;
                case 2:
                    type = 2;
                    break;
                case 3:
                    type = 3;
                    break;
                case 4:
                    type = 4;

                    bossY = -1500;
                    bossX = rand.nextInt((GamePanel.WIDTH / 3) - 250) + 250; // Obergrenze 500 Untergrenze 250
                    break;

            }

    }

    public void testCreateBoss() {
        int zz = rand.nextInt(rarity)+1;
        int zz2 = rand.nextInt(rarity)+1;
        if(zz == zz2) {
            if(!BossMeteors.bossfight) {
                meteorHitbox.clear();
                BossMeteors.bossfight = true;
            }

        }
    }

    public void draw(Canvas canvas) {

    switch (type)
    {
        case 1: canvas.drawBitmap(boss1, bossX, bossY, null); break;
        case 2: canvas.drawBitmap(boss2, bossX, bossY, null); break;
        case 3: canvas.drawBitmap(boss3, bossX, bossY, null); break;
        case 4: canvas.drawBitmap(boss4,bossX,bossY,null);    break;

    }


    }


    }







