package com.bergmann.matthias.androidgame;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;


public class Game extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(new GamePanel(this, this));


    }

    public void startPauseScreen() {
        startActivity(new Intent(this,PauseMenu.class));
    }

    public void startGame() {
        setContentView(new GamePanel(this, this));
    }


    public void stop(){
        startActivity(new Intent(this, MainMenu.class));
    }




}
