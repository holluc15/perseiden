package com.bergmann.matthias.androidgame;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainMenu extends AppCompatActivity {

    public static MediaPlayer ingameSound;
    public static MediaPlayer deathSound;
    public static MediaPlayer startscreensound;
    public static SharedPreferences sharedPref;
    public static Context context;
    public static SharedPreferences.Editor prefEditor;

    public PopupWindow settingsWindow;
    private ImageButton startBt;
    private ImageButton settingsBt;
    private ImageButton creditsBt;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // turn title off
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // make game fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ingameSound = MediaPlayer.create(this, R.raw.ingamesound);
        deathSound = MediaPlayer.create(this, R.raw.deathsound);
        startscreensound = MediaPlayer.create(this, R.raw.startscreensound);

        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        Player.changeCtrl(sharedPref.getInt(getString(R.string.ctrlKey), 1));
        //System.out.printf("ctrl: %d\n", sharedPref.getInt(getString(R.string.ctrlKey), 1));
        Sounds.playStartScreenSound();
        setContentView(R.layout.activity_mainmenu);

        context = getApplicationContext();
        startBt = (ImageButton) findViewById(R.id.button_start);
        settingsBt = (ImageButton) findViewById(R.id.button_settings);
        creditsBt = (ImageButton) findViewById(R.id.button_credits);

        prefEditor = getPreferences(Context.MODE_PRIVATE).edit();



        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void start(View v) {
        //startBt.setImageResource(R.drawable.button_start_clicked);

        startActivity(new Intent(this, Game.class));
        Sounds.stopStartScreenSound();

    }

    public void changeProgress(SeekBar seekBar, int progress){
        try {
            int[] imageID = {R.drawable.vol0, R.drawable.vol1, R.drawable.vol2, R.drawable.vol3, R.drawable.vol4, R.drawable.vol5};
            seekBar.setBackground(ContextCompat.getDrawable(this, imageID[progress]));
            Sounds.setVolume(progress, Sounds.maxVolume());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void settings(View v) {
        //settingsBt.setImageResource(R.drawable.button_start);

       startActivity(new Intent(this,Settings.class));
    }

    public void credits(View v) {
        //creditsBt.setImageResource(R.drawable.clicked_button_credits);

        startActivity(new Intent(this,Credits.class));
    }

    public static void oneHand() {
            prefEditor.putInt(context.getString(R.string.ctrlKey), Player.ONE_HAND_CONTROL);
            Player.changeCtrl(Player.ONE_HAND_CONTROL);
            prefEditor.apply();
    }

    public static void twoHand() {
            prefEditor.putInt(context.getString(R.string.ctrlKey), Player.TWO_HAND_CONTROL);
            Player.changeCtrl(Player.TWO_HAND_CONTROL);
            prefEditor.apply();
    }


    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("MainMenu Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }


    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
