package com.bergmann.matthias.androidgame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PauseMenu extends AppCompatActivity {


    public static boolean pause = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pause_menu);
        pause = true;
    }
}
