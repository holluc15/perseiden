package com.bergmann.matthias.androidgame.GUI.Items;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import java.util.LinkedList;
import java.util.Random;

import android.graphics.Paint;
import android.graphics.Rect;

import com.bergmann.matthias.androidgame.GUI.Character.Player;
import com.bergmann.matthias.androidgame.GUI.Meteor.BossMeteors;
import com.bergmann.matthias.androidgame.GUI.GamePanel;
import com.bergmann.matthias.androidgame.GUI.Meteor.Meteor;

public class Powerup {

    private Rect hitboxPlayerHead;
    private Rect hitboxPlayerBody;


    private int speed = 5; // speed could be changed
    private int screenW;
    private int screenH;
    public static boolean powerUpActive = false;
    private Rect powerUpHitbox;
    private int count = 0;
    private int timer = 0;
    private int FPS;
    private int rarity = 550;
    public static boolean powerupAction = false;
    Paint paint = new Paint();

    private LinkedList<Bitmap> images = new LinkedList<>();
    // [0] -> BossPowerUp
    // [1] -> FasterPowerUp
    // [2] -> SlowerPowerUp
    // [3] -> IncreasePowerUp
    // [4] -> ReducePowerUp

    Random rand = new Random();
    private int powerUpX = 200;
    private int powerUpY = 200;
    private int type;


    public Powerup(LinkedList<Bitmap> list, int width,int height, int FPS) {
        this.screenH = height;
        this.screenW = width;
        this.images.addAll(list);
        type = rand.nextInt(images.size());
        this.FPS = FPS;

    }

    public void updateHitboxes() {
        powerUpHitbox = new Rect(powerUpX + 15,powerUpY +15 ,powerUpX +140,powerUpY + 140);
        hitboxPlayerHead = GamePanel.hitboxPlayerHead;
        hitboxPlayerBody = GamePanel.hitboxPlayerBody;
    }



    public void movePowerup() {
        if(Powerup.powerUpActive) {

            powerUpY += speed;
            testOutside();
            testHitPlayerByPowerUp();

        }
    }

    public void testOutside() {

        if (powerUpY > screenH){
            Powerup.powerUpActive = false;

        }
    }


    public void updateAction() {
        if(powerupAction) {

            switch (type) {
                case 0:
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                    timer++;
                    System.out.println("TIMER **" + type);
                    if (timer >= 100) {
                        deletePowerupAction();
                    }

                    break;
                default:
                    System.out.println("Wrong type of powerup:(in updateAction method) " + type);
            }
        }
    }

    public void deletePowerupAction() {
        Meteor.speed = 5;
        Player.speed = 300 / FPS;
        timer = 0;
        powerUpActive = false;
        powerupAction = false;

    }

        public void testHitPlayerByPowerUp() {
            updateHitboxes();
            if (hitboxPlayerHead.intersect(powerUpHitbox) || hitboxPlayerBody.intersect(powerUpHitbox)) {

                Powerup.powerUpActive = false;
                switch (type) {

                    case 1:
                        Meteor.speed = Meteor.speed + 3;
                        break;
                    case 2:
                        Meteor.speed = Meteor.speed - 2;
                        break;
                    case 3: Player.speed = Player.speed + 3;
                        break;
                    case 4: Player.speed = Player.speed - 3;
                        break;

                    default:
                        System.out.println("Wrong type of powerup:(in collisionByPowerUp method) " + type);
                }
                timer = 0;
                powerupAction = true;

            }

        }


    public void createPowerUp() {

        powerUpX = rand.nextInt((screenW-100)-100) + 100;
        powerUpY = rand.nextInt((screenH - screenH/2)-100) + 100;
    }



    public void testCreatePowerUp() {

        int zz = rand.nextInt(rarity)+1;
        int zz2 = rand.nextInt(rarity)+1;
        if(zz == zz2) {
            if(!BossMeteors.bossfight && !powerUpActive && !powerupAction) {
                Powerup.powerUpActive = true;
                createPowerUp();
            }

        }
    }

    public void draw(Canvas canvas) {

        if (powerUpActive) {
            if(count >= speed) {

                if(type >= images.size() -1) {
                    type = 0;
                }else{
                    type ++;
                }
                count = 0;
            }else{
                count ++;
            }
            canvas.drawBitmap(images.get(type), powerUpX, powerUpY, null);
        }

    }


}
