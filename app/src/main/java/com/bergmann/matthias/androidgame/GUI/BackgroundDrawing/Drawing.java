package com.bergmann.matthias.androidgame.GUI.BackgroundDrawing;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.bergmann.matthias.androidgame.GUI.Game;
import com.bergmann.matthias.androidgame.GUI.GamePanel;
import com.bergmann.matthias.androidgame.GUI.Meteor.BossMeteors;

import static com.bergmann.matthias.androidgame.Menu.MainMenu.context;


public class Drawing {


    private Bitmap background;
    private Bitmap bossbackground;
    private Bitmap pauseIcon;
    Paint paint = new Paint();
    Game game;
    Typeface fontCooperBlack = Typeface.createFromAsset(context.getAssets(), "fonts/bad_mofo.ttf");

    public Drawing(Bitmap background,Bitmap pauseIcon,Bitmap bossbackground) {
        // height is also width/10 because icon is distorted otherwise

        this.background = Bitmap.createScaledBitmap(background, GamePanel.WIDTH, GamePanel.HEIGHT, false);
        this.pauseIcon = Bitmap.createScaledBitmap(pauseIcon, GamePanel.WIDTH/15, GamePanel.HEIGHT/15, false);
        this.bossbackground = Bitmap.createScaledBitmap(bossbackground, GamePanel.WIDTH, GamePanel.HEIGHT, false);
        game = new Game();
    }
    public void showFPS(Canvas canvas, double FPS) {

        paint.setColor(Color.WHITE);
        paint.setTextSize(60);
        paint.setFakeBoldText(true);
        paint.setLinearText(true);
        paint.setTypeface(fontCooperBlack);
        canvas.drawText(FPS + " FPS",30,60,paint);

    }

    public void drawScore(Canvas canvas, long score) {

        paint.setColor(Color.WHITE);
        paint.setTextSize(65);
        paint.setFakeBoldText(true);
        paint.setLinearText(true);
        paint.setTypeface(fontCooperBlack);

        canvas.drawText("score: " + score,GamePanel.WIDTH/2-160,60,paint);
    }

    public void drawHighscore(Canvas canvas,int highscore) {
        paint.setColor(Color.WHITE);
        paint.setTextSize(65);
        paint.setFakeBoldText(true);
        paint.setLinearText(true);
        paint.setTypeface(fontCooperBlack);
        canvas.drawText("Highscore: " + highscore,GamePanel.WIDTH-470,60,paint);
    }

    public void drawBackground(Canvas canvas){

        if(!BossMeteors.bossfight) {
            canvas.drawBitmap(background, 0, 0, null);
        }else{
            canvas.drawBitmap(bossbackground, 0, 0, null);
        }

    }

    public void drawPauseIcon(Canvas canvas) {
        canvas.drawBitmap(pauseIcon,GamePanel.WIDTH - 110, 10 ,null);

    }

    public void drawEarth(Canvas canvas, Bitmap image) {
        canvas.drawBitmap(image,75,75,null);
    }




}
