package com.bergmann.matthias.androidgame;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;

import com.bergmann.matthias.androidgame.Animation.Animation;

public class Player {
    private Animation animation = new Animation();
    private Animation deathAnimation = new Animation();
    private Animation smokeAnimation = new Animation();

    // saves x and y coordinates
    public static double x;
    public static double y;
    // saves how fast to change x, y and angle (d stands for delta)
    private double dx;
    private double dy;
    private double da;

    // saves screenwidth and screenheight to keep character within screen
    private int screenWidth;
    private int screenHeight;

    // saves the width and height of the character
    private int width;
    private int height;
    private int widthDeath;
    private int heightDeath;
    private double diagonal, canvasDiagonal;

    // saves how far the character is turned
    public int angle;
    // saves how many lives the character has left
    private int lives;
    public static boolean explode;

    private int FPS;

    // this is the speed at which the character can move
    public static double speed;
    private double turnSpeed = 90;

    // constants for animation frames
    private final static int RIGHT = 0;
    private final static int LEFT = 1;
    private final static int DOWN = 2;
    private final static int UP = 3;
    private final static int RIGHT_UP = 4;
    private final static int LEFT_UP = 5;
    private final static int RIGHT_DOWN = 6;
    private final static int LEFT_DOWN = 7;
    private final static int NORMAL = 8;
    private final static int FRAMES = 9;

    public final static int TWO_HAND_CONTROL = 2;
    public final static int ONE_HAND_CONTROL = 1;
    // this saves how the character is controlled
    private static int ctrl;
    public static void changeCtrl(int control){ctrl = control;}
    public static int getCtrl(){return ctrl;}

    public Player(Bitmap res, Bitmap[] deathRes, Bitmap[] smokeRes, int screenW, int screenH, int FPS){
        Bitmap image = Bitmap.createScaledBitmap(res, screenW/8*FRAMES, screenW/8, false);

        screenWidth = screenW;
        screenHeight = screenH;

        this.FPS = FPS;

        // getHeight because there are 9 images in a row, each square so only height is important
        height = image.getHeight();
        width = image.getWidth()/FRAMES;
        diagonal = Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
        canvasDiagonal = Math.sqrt(Math.pow(width*2, 2) + Math.pow(height*2, 2));

        x = screenWidth/2 - width/2;
        y = screenHeight/2 - height/2;
        dx = 0;
        dy = 0;
        angle = 0;
        da = 0;
        lives = 10;
        speed = 300 / this.FPS;
        turnSpeed /= this.FPS;
        explode = false;

        Bitmap[] imageFrames = new Bitmap[FRAMES];
        for (int i = 0; i < FRAMES; i++) {
            imageFrames[i] = Bitmap.createBitmap(image, width*i, 0, width, height);
        }
        animation.setFrames(imageFrames);
        animation.setFrame(NORMAL);

        widthDeath = width * 3;
        heightDeath = height * 3;
        imageFrames = deathRes;
        for (int i = 0; i < imageFrames.length; i++) {
            imageFrames[i] = Bitmap.createScaledBitmap(imageFrames[i], widthDeath, heightDeath, false);
        }
        deathAnimation.setFrames(imageFrames);
        deathAnimation.setDelay(30);

        // divisor is just for easier adjustment
        int divisor = 5;

        int smokeWidth = width/divisor;
        int smokeHeight = width/divisor*(smokeRes[0].getHeight()/smokeRes[0].getWidth());

        imageFrames = smokeRes;
        for (int i = 0; i < imageFrames.length; i++) {
            try {
                imageFrames[i] = Bitmap.createScaledBitmap(imageFrames[i], smokeWidth, smokeHeight, false);
            }catch(NullPointerException ex) {
                System.out.println("EXCEPTION!");
            }
        }
        smokeAnimation.setFrames(imageFrames);
        smokeAnimation.setDelay(20);
    }

    public Bitmap RotateImage(Bitmap res, int angle){
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(res, 0, 0, res.getWidth(), res.getHeight(), matrix, true);
    }

    public void move(int refX, int refY, int tarX, int tarY){
        if(ctrl == TWO_HAND_CONTROL) {
            // calculate distance for later use with the theorem of pythagoras
            double distY = tarY - refY;
            double dist = (int) Math.sqrt(Math.pow(refX - tarX, 2) + Math.pow(distY, 2));

            if (GamePanel.movePlayer) {
                // speed is calculated with the current angle and simple trigonometry
                dx = Math.sin(Math.toRadians(angle)) * speed;
                dy = -Math.cos(Math.toRadians(angle)) * speed;
            }
            if (dist == 0) {
                da = 0;
                animation.setFrame(NORMAL);
                return;
            }

            // calculate the angle of the refpoint and tarpoint
            // the angles are now →=0   ↓= 90 ←0   ↑=-90
            int controlAngle = (int) Math.toDegrees(Math.asin(distY / dist));

            // change it so it goes from 0-360
            if (refX <= tarX && refY >= tarY) {
                controlAngle = -90 - controlAngle;
                controlAngle *= -1;
            } else if (refX <= tarX && refY <= tarY) {
                controlAngle += 90;
            } else if (refX >= tarX && refY <= tarY) {
                controlAngle = 180 + (90 - controlAngle);
            } else {
                controlAngle = 270 - controlAngle;
            }

            controlAngle += (360 - this.angle);
            while (controlAngle >= 360) {
                controlAngle -= 360;
            }

            // if control Angle is > 0 and < 180 player has to turn right(positive)
            // if control angle is > 180 player has to turn left (negative)
            if (controlAngle > 5 && controlAngle < 180) {
                da = turnSpeed;
                if (controlAngle > 30) {
                    animation.setFrame(RIGHT);
                }
            } else if (controlAngle >= 180 && controlAngle < 355) {
                da = -turnSpeed;
                if (controlAngle < 330) {
                    animation.setFrame(LEFT);
                }
            } else {
                da = 0;
                animation.setFrame(NORMAL);


            }
        }
        else {
            // calculate distance
            double dist = (int) Math.sqrt(Math.pow(refX - tarX, 2) + Math.pow(refY - tarY, 2));
            double distX = tarX - refX;
            double distY = tarY - refY;

            // dx is simply the difference of refx & tarX divided by (dist/speed)
            if (dist != 0) {
                dx += ((distX) / (dist / (speed/3)));
                dy += ((distY) / (dist / (speed/3)));
                // dx and dy should not  be higher than speed or lower than -speed
                if (dx > speed) {
                    dx = speed;
                }
                if (dx < -speed) {
                    dx = -speed;
                }
                if (dy > speed) {
                    dy = speed;
                }
                if (dy < -speed) {
                    dy = -speed;
                }

            } else {
                da = 0;
                animation.setFrame(NORMAL);
                return;
            }

            // calculate the angle of the refpoint and tarpoint
            // the angles are now →=0   ↓= 90 ←0   ↑=-90
            int controlAngle = (int) Math.toDegrees(Math.asin(distY / dist));

            // change it so it goes from 0-360
            if (refX <= tarX && refY >= tarY) {
                controlAngle = -90 - controlAngle;
                controlAngle *= -1;
            } else if (refX <= tarX && refY <= tarY) {
                controlAngle += 90;
            } else if (refX >= tarX && refY <= tarY) {
                controlAngle = 180 + (90 - controlAngle);
            } else {
                controlAngle = 270 - controlAngle;
            }

            controlAngle += (360 - this.angle);
            while (controlAngle >= 360) {
                controlAngle -= 360;
            }

            // if control Angle is > 0 and < 180 player has to turn right(positive)
            // if control angle is > 180 player has to turn left (negative)
            if (controlAngle > 5 && controlAngle < 180) {
                da = turnSpeed;
                if (controlAngle > 30) {
                    animation.setFrame(RIGHT);
                }
            } else if (controlAngle >= 180 && controlAngle < 355) {
                da = -turnSpeed;
                if (controlAngle < 330) {
                    animation.setFrame(LEFT);
                }
            } else {
                da = 0;
                animation.setFrame(NORMAL);

            }
        }
    }

    public void update(int refX, int refY, int tarX, int tarY){
        if (explode) return;

        // update dx, dy and da
        move(refX, refY, tarX, tarY);


        // update angle and position
        angle += da;
        x += dx;
        y += dy;
        if (!GamePanel.movePlayer) {
            dx *= 0.95;
            dy *= 0.95;
        }

        // make sure it stays within screen
        if (x < 0){
            x = 0;
            dx *= -0.3;
        }
        else if (x > screenWidth - width){
            x = screenWidth-width;
            dx *= -0.3;
        }
        if (y < 0){
            y = 0;
            dy *= -0.3;
        }
        else if (y > screenHeight - height){
            y = screenHeight-height;
            dy *= -0.3;
        }

        if (angle >= 360){angle -= 360;}
        if (angle < 0){angle += 360;}
    }

    public void draw(Canvas canvas){
        if (!explode) {
            // create empty canvas and draw player and smoke on it
            Bitmap bmp = Bitmap.createBitmap(width*2, height*2, Bitmap.Config.ARGB_8888);
            Canvas emptyCanvas = new Canvas(bmp);

            // determine the position of the smoke animation depending on the frame
            int posX = width, posY = height;
            switch(animation.getFrame()){
                case NORMAL:
                    posX *= 1.0/7.0;
                    posY *= 6.0/7.0;
                    break;
                case UP:
                    posX *= 1.0/7.0;
                    posY *= 6.0/7.0;
                    break;
                case RIGHT:
                    posX *= 1.0/7.0;
                    posY *= 6.0/7.0;
                    break;
                case LEFT:
                    posX *= 5.0/7.0;
                    posY *= 6.0/7.0;
                    break;
                default:
                    System.out.println("unknown frame");
            }
            int offsetX = (int) (width*0.5), offsetY = (int) (height*0.5);
            // draw the player central onto the canvas
            emptyCanvas.drawBitmap(animation.getImage(), offsetX, offsetY, null);

            // draw the smoke onto the player
            emptyCanvas.drawBitmap(smokeAnimation.getImage(), posX+offsetX, posY+offsetY, null);

            // calculate offset so that image is steady when turned
            int offset = (int) ((canvasDiagonal - bmp.getWidth()) * Math.abs(Math.sin(Math.toRadians(angle * 2)))) / 2;

            // draw the canvas with player and smoke
            canvas.drawBitmap(RotateImage(bmp, angle), (int) x - offset - offsetX, (int) y - offset - offsetY, null);
            // update the smoke animation
            smokeAnimation.update();
        }
        else{
            // calculate offset so that image is steady when turned
            int offset = (int) (((diagonal - width) * Math.abs(Math.sin(Math.toRadians(angle * 2)))) / 2 * 3) + widthDeath/3;

            // draw and update the explosion
            canvas.drawBitmap(RotateImage(deathAnimation.getImage(), angle), (int) x - offset, (int) y - offset, null);
            deathAnimation.update();

            // if the explosion has been run once the gameloop should be stopped
            if (deathAnimation.playedOnce()) {
                MainThread.running = false;
            }
        }
    }

    public int getWidth(){return width;}
    public int getHeight(){return height;}
}
