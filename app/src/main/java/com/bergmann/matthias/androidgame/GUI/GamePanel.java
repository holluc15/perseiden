
package com.bergmann.matthias.androidgame.GUI;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.bergmann.matthias.androidgame.Animation.Animation;
import com.bergmann.matthias.androidgame.GUI.BackgroundDrawing.Drawing;
import com.bergmann.matthias.androidgame.GUI.Items.Powerup;
import com.bergmann.matthias.androidgame.GUI.Meteor.BossMeteors;
import com.bergmann.matthias.androidgame.Thread.MainThread;
import com.bergmann.matthias.androidgame.Menu.PauseMenu;
import com.bergmann.matthias.androidgame.GUI.Meteor.Meteor;
import com.bergmann.matthias.androidgame.GUI.Character.Player;
import com.bergmann.matthias.androidgame.R;
import com.bergmann.matthias.androidgame.Data.SaveData;
import com.bergmann.matthias.androidgame.MySound.Sounds;
import com.bergmann.matthias.androidgame.GUI.Meteor.TrackMeteor;
import com.bergmann.matthias.androidgame.GUI.Character.updateHitboxPlayer;

import java.util.LinkedList;

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback{
    // this is basically just the width of the background that is used.
    public final static int WIDTH = 1500;
    public final static int HEIGHT = 1000;
    public final static int FPS = 30;

    public static MainThread thread;
    private Player player;
    private Meteor meteor;
    private Powerup powerup;
    private BossMeteors boss;
    private Animation earthAnimation;



    private Drawing drawing;

    private static Game game;
    private TrackMeteor trackmeteor;



    public Game getGame(){return game;}
    private long mtDelayTime;
    public static boolean trackMeteorActive = false;


    private int refX;
    private int refY;
    private int tarX;
    private int tarY;
    public static boolean movePlayer = false;
    public static int movePlayerid = 1000;
    public static Rect hitboxPlayerHead;
    public static Rect hitboxPlayerBody;
    private boolean first = true;
    private boolean firstTrack = true;
    public static SaveData pc;
    private int touchCounter = 0;
    private LinkedList<Bitmap> images;
    private int index = 10;
    public static boolean onTouch = false;



    public GamePanel(Context context, Game g){
        super(context);

    //add the callback to the surfaceholder to intercept events
        getHolder().addCallback(this);
        resetBooleans();
    // initiate variables

    images = new LinkedList<>();
    initImages();

    player = new Player(images.get(images.size() - 3), loadDeathBitmap(), loadSmokeBitmap(),loadSmokeActiveBitmap(), WIDTH, HEIGHT, FPS);

    meteor = new Meteor(images.get(2), images.get(3), images.get(4), images.get(5), WIDTH, HEIGHT, FPS);

    boss = new BossMeteors(images.get(6), images.get(7), images.get(8), images.get(9));

    mtDelayTime = System.currentTimeMillis();

    powerup = new Powerup(loadPowerups(), WIDTH, HEIGHT, FPS);

    drawing = new Drawing(images.get(0), images.get(images.size()-2),images.get(images.size()-1));

    game = g;



    pc = new SaveData(context);

    trackmeteor = new TrackMeteor(images.get(1));

        earthAnimation = new Animation();
        earthAnimation.setFrames(loadEarthBitmap());
        earthAnimation.setDelay(85);

    }


    public LinkedList<Bitmap> initImages() {
        /*
            [0] background
            [1] trackMeteor
            [2 - 5] meteors
            [6 - 9] bossMeteors
            [10 - 41] explosionFrames
            [42 - 62] smokeFrames
            [63 - 68] bossFrames
            [69] completeSpritesheet
            [70 - 254] earthAnimation
        */
        int[] explosionID = {
                R.drawable.explosion0,
                R.drawable.explosion1,
                R.drawable.explosion2,
                R.drawable.explosion3,
                R.drawable.explosion4,
                R.drawable.explosion5,
                R.drawable.explosion6,
                R.drawable.explosion7,
                R.drawable.explosion8,
                R.drawable.explosion9,
                R.drawable.explosion10,
                R.drawable.explosion11,
                R.drawable.explosion12,
                R.drawable.explosion13,
                R.drawable.explosion14,
                R.drawable.explosion15,
                R.drawable.explosion16,
                R.drawable.explosion17,
                R.drawable.explosion18,
                R.drawable.explosion19,
                R.drawable.explosion20,
                R.drawable.explosion21,
                R.drawable.explosion22,
                R.drawable.explosion23,
                R.drawable.explosion24,
                R.drawable.explosion25,
                R.drawable.explosion26,
                R.drawable.explosion27,
                R.drawable.explosion28,
                R.drawable.explosion29,
                R.drawable.explosion30 };

        int[] smokeID = {
                R.drawable.smoke1,
                R.drawable.smoke2,
                R.drawable.smoke3,
                R.drawable.smoke4,
                R.drawable.smoke5,
                R.drawable.smoke6,
                R.drawable.smoke7,
                R.drawable.smoke8,
                R.drawable.smoke9,
                R.drawable.smoke10,
                R.drawable.smoke11,
                R.drawable.smoke12,
                R.drawable.smoke13,
                R.drawable.smoke14,
                R.drawable.smoke15,
                R.drawable.smoke16,
                R.drawable.smoke17,
                R.drawable.smoke18,
                R.drawable.smoke19,
                R.drawable.smoke20,

        };

        int[] powerupID = {
                R.drawable.boss,
                R.drawable.reduce,
                R.drawable.faster,
                R.drawable.slower,
                R.drawable.increase
        };

        int[] earthID = {
                R.drawable.planet000
        };

        int[] smokeActiveID = {
                R.drawable.smoke16,
                R.drawable.smoke17,
                R.drawable.smoke18,
                R.drawable.smoke19,
                R.drawable.smoke20
        };



        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.background));
        images.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.meteorite), WIDTH/8, WIDTH/8, false));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.meteorite));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.meteorite2));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.meteorite3));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.meteorite4));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.boss1));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.boss2));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.boss3));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.boss4));

        for (int i = 0; i < explosionID.length; i++) {
                images.add(BitmapFactory.decodeResource(getResources(), explosionID[i]));
        }

        for(int j = 0; j < smokeID.length; j++) {
            images.add(BitmapFactory.decodeResource(getResources(), smokeID[j]));
        }

        for(int j = 0; j < smokeActiveID.length; j++) {
            images.add(BitmapFactory.decodeResource(getResources(), smokeActiveID[j]));
        }


        for(int k = 0; k < powerupID.length; k++) {
            images.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),powerupID[k]), WIDTH/12, WIDTH/12, false));
        }

        for(int y = 0; y < earthID.length; y++) {
            images.add(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),earthID[y]), WIDTH/3, WIDTH/3, false));
        }
        images.add(BitmapFactory.decodeResource(getResources(), R.drawable.completesprite));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.settings_icon));
        images.add(BitmapFactory.decodeResource(getResources(),R.drawable.background_boss));


        return images;
    }


    public void resetMemory() {

        for(int i = 0; i < images.size(); i++) {
            if (images.get(i) != null) {
                images.get(i).recycle();
                images.set(i, null);
            }
        }
        player = null;
        meteor = null;
        boss = null;
        powerup = null;
        drawing = null;
        game = null;
        pc = null;
        trackmeteor = null;
        thread =null;
        earthAnimation = null;



    }

    public void resetBooleans() {
        Powerup.powerUpActive = false;
        Powerup.powerupAction = false;
        GamePanel.trackMeteorActive = false;
        BossMeteors.bossfight = false;
    }

    public Bitmap[] loadDeathBitmap() {
        Bitmap[] imageFrames = new Bitmap[31];
        for(int i = 0; i < imageFrames.length; i++) {
            imageFrames[i] = images.get(index);
            index++;
        }
        return imageFrames;
    }

    public Bitmap[] loadSmokeBitmap() {
        Bitmap[] imageFrames = new Bitmap[20];
        for(int i = 0; i < imageFrames.length; i++) {
            imageFrames[i] = images.get(index);
            index++;
        }
        return imageFrames;
    }

    public Bitmap[] loadEarthBitmap() {
        Bitmap[] imageFrames = new Bitmap[1];
        for(int i = 0; i < imageFrames.length; i++) {
            imageFrames[i] = images.get(index);
            index++;
        }
        return imageFrames;
    }

    public Bitmap[] loadSmokeActiveBitmap() {
        Bitmap[] imageFrames = new Bitmap[5];
        for(int i = 0; i < imageFrames.length; i++) {
            imageFrames[i] = images.get(index);
            index++;
        }
        return imageFrames;
    }

    public LinkedList<Bitmap> loadPowerups() {
        LinkedList<Bitmap> imageFrames = new LinkedList<>();
        for(int i = 0; i < 5; i++) {
            imageFrames.add(images.get(index));
            index++;
        }
        return imageFrames;
    }

    public static void death() {

        Player.explode = true;
        MainThread.multiplier = 1;

        Meteor.speed = 5;
        Player.speed = 300 / GamePanel.FPS;
        MainThread.gamePoints = 0;
        MainThread.difficulty = 75;
        Sounds.stopIngameSound();
        Sounds.playDeathSound();

        if(MainThread.score > pc.getHighscore()) {
            pc.setHighscore(MainThread.score);

        }


        MainThread.score = 0;

    }




    public void pauseIconClicked(float x, float y) {
        if(y <= 70 && x >= WIDTH-400) {
            if(PauseMenu.pause != true) {
                game.startPauseScreen();
            }


        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder){
        thread = new MainThread(getHolder(), this);

        //we can safely start the game loop
        thread.setRunning(true);
        thread.start();

    }
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){}
    @Override
    public void surfaceDestroyed(SurfaceHolder holder){
        // thread.join doesn't always work on the first try, timeout at 1000 tries
        boolean retry = true;
        int timeout = 0;

        while(retry && timeout < 1000)
        {
            timeout++;
            try{thread.setRunning(false);
                thread.join();
                retry = false;
                thread = null;
            }catch(InterruptedException e){e.printStackTrace();}
        }
        resetMemory();

    }



    public void update(){
        // call update methods of objects

        player.update(refX, refY, tarX, tarY);
        hitboxPlayerHead = updateHitboxPlayer.updateHitboxPlayerHead(player);
        hitboxPlayerBody = updateHitboxPlayer.updateHitboxPlayerBody(player);


        if (System.currentTimeMillis() - mtDelayTime > 1000) {

            mtDelayTime = System.currentTimeMillis();
        }

        //update
        if(trackMeteorActive) {
            if(firstTrack) {
                trackmeteor.createTrackMeteor();
                firstTrack = false;
            }
            trackmeteor.moveTrackMeteor();
            trackmeteor.testOutside();
            trackmeteor.updateTrackHitbox();
            trackmeteor.testHitPlayer();
        }else{
            firstTrack = true;
        }

        meteor.createMeteor();
        meteor.moveMeteor();
        meteor.testHitPlayer();

        if(Powerup.powerUpActive) {
            powerup.movePowerup();
            powerup.testHitPlayerByPowerUp();

        }else{
            powerup.updateAction();
            powerup.testCreatePowerUp();
        }

        if(BossMeteors.bossfight) {
            if(first) {
                boss.createMeteor();
                first = false;
            }

            boss.moveBossMeteor();
            boss.updateBossHitbox();
            boss.testHitPlayer();

        }else{
            first = true;
            boss.testCreateBoss();
        }

    }

    @Override
    public void draw(Canvas canvas){
        try {
            super.draw(canvas);
        }catch(NullPointerException ex) {
            System.exit(0);
        }

        // scale canvas for different screen sizes
        final float scaleFactorX = (float)getWidth()/WIDTH;
        final float scaleFactorY = (float)getHeight()/HEIGHT;

        if (canvas != null) {
            canvas.scale(scaleFactorX, scaleFactorY);
            // call draw methods of objects


            drawing.drawBackground(canvas);
            drawing.drawEarth(canvas,earthAnimation.getImage());
            earthAnimation.update();
           // System.out.println("Frame anzahl:" + earthAnimation.getNumFrames() + " spielt gerade ab: " + earthAnimation.getFrame() + " Delay: " + earthAnimation.getDelay());


            player.draw(canvas);
            powerup.draw(canvas);
            if(BossMeteors.bossfight) {
                boss.draw(canvas);
            }
            meteor.draw(canvas);
            if(trackMeteorActive) {
                trackmeteor.draw(canvas);
            }

            drawing.showFPS(canvas,MainThread.averageFPS);
            drawing.drawScore(canvas,MainThread.score);
            drawing.drawHighscore(canvas,pc.getHighscore());
            drawing.drawPauseIcon(canvas);


            // draw a "joystick" between refPoint and tarPoint
            if(refX !=tarX || refY != tarY) {
                Paint paint = new Paint();
                paint.setColor(Color.GRAY);
                paint.setAlpha(50);
                paint.setStrokeWidth(5);

                canvas.drawCircle(refX / scaleFactorX, refY / scaleFactorY, 40, paint);
                canvas.drawLine(refX / scaleFactorX, refY / scaleFactorY, tarX / scaleFactorX, tarY / scaleFactorY, paint);
            }
        }
    }


    public void resetTouchTimer() {
        touchCounter = 0;
    }

    public int getTouchCounter() {
        return touchCounter;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        int action = event.getActionMasked();
        int index = event.getActionIndex();
        int id = event.getPointerId(index);
        float x = event.getX();
        float y = event.getY();

        switch (action){

            case MotionEvent.ACTION_DOWN:
                // do whatever should be done when first pointer touches the screen
                onTouch = true;
                pauseIconClicked(x,y);
                touchCounter++;
                if (event.getX(index) > WIDTH/2 || Player.getCtrl() == 2) {
                    // save touchpoint for movement control
                    refX = (int) event.getX(index);
                    refY = (int) event.getY(index);
                    tarX = refX;
                    tarY = refY;
                }
                else{
                    movePlayer = true;
                    movePlayerid = id;

                }
                return true;


            case MotionEvent.ACTION_POINTER_DOWN:
                if(Player.getCtrl() == 2){return true;}
                // do whatever should be done at action pointer down event
                if (event.getX(index) > WIDTH/2) {
                    // save touchpoint for movement control
                    refX = (int) event.getX(index);
                    refY = (int) event.getY(index);
                    tarX = refX;
                    tarY = refY;
                }
                else{
                    movePlayer = true;
                    movePlayerid = id;

                }
                return true;

            case MotionEvent.ACTION_MOVE:
                // do whatever should be done at action move event
                // change tar touchpoint for movement control
                if (id != movePlayerid || Player.getCtrl() == 2) {
                    tarX = (int) event.getX(index);
                    tarY = (int) event.getY(index);
                }
                return true;

            case MotionEvent.ACTION_UP:
                // do whatever should be done when last pointer leaves screen
                tarX = refX;
                tarY = refY;
                movePlayer = false;
                movePlayerid = 1000;

               onTouch = false;

                return true;

            case MotionEvent.ACTION_POINTER_UP:
                if(Player.getCtrl() == 2){return true;}
                // do whatever should be done at action up event
                if (id == movePlayerid) {
                    movePlayer = false;
                    movePlayerid = 1000;
                }
                else{
                    tarX = refX;
                    tarY = refY;
                }

        }

        return super.onTouchEvent(event);
    }





}