package com.bergmann.matthias.androidgame.Animation;

import android.graphics.Bitmap;

public class Animation {
    private Bitmap[] frames;
    private int currentFrame;
    private long startTime;
    private long delay;
    private boolean playedOnce = false;

    public void setFrames(Bitmap[] frames){
        this.frames = frames;
        currentFrame = 0;
        startTime = System.nanoTime();
    }

    public void setDelay(long d){delay = d;}
    public void setFrame(int i){currentFrame = i;}
    public void setPlayedOnce(boolean playedOnce){this.playedOnce = playedOnce;}
    public long getDelay(){return delay;}
    public int getFrame(){return currentFrame;}
    public int getNumFrames(){return frames.length;}
    public boolean playedOnce(){return playedOnce;}
    public Bitmap getImage(){return frames[currentFrame];}

    public void update(){

        long elapsed = (System.nanoTime()-startTime)/1000000;
        if(elapsed > delay){
            startTime = System.nanoTime();
            currentFrame++;
        }
        if(currentFrame == frames.length){
            playedOnce = true;
            currentFrame = 0;
        }
    }
}